using System.Threading.Tasks;
using DasaPlanet.Core;
using DasaPlanet.Models;
using DasaPlanet.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DasaPlanet.Controllers
{
    [AuthorizeToken]
    [ApiController]
    [Route("[controller]")]
    public class PlayersController : ControllerBase
    {
        private readonly DasaPlanetContext _Context;
        private readonly IPlayerRepository _PlayerRepository;

        public PlayersController(DasaPlanetContext context, IPlayerRepository playerRepository)
        {
            _Context = context;
            _PlayerRepository = playerRepository;
        }

        [AuthorizeToken(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlayer(int id, Player player)
        {
            if (id != player.Id)
            {
                return BadRequest();
            }

            _Context.Entry(player).State = EntityState.Modified;

            try
            {
                await _Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(await(_PlayerRepository.ExistAsync(p => p.Id == id))))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [AuthorizeToken(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Player>> DeletePlayer(int id)
        {
            var player = await _PlayerRepository.GetPlayerByIdAsync(id);
            if (player == null)
            {
                return NotFound();
            }

            _PlayerRepository.Delete(player);
            await _Context.SaveChangesAsync();

            return player;
        }
    }
}