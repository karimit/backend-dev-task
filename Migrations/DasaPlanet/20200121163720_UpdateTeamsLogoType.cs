﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DasaPlanet.Migrations.DasaPlanet
{
    public partial class UpdateTeamsLogoType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "logo",
                table: "teams",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "bytea",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "logo",
                table: "teams",
                type: "bytea",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
