using DasaPlanet.Core;
using Microsoft.EntityFrameworkCore;

namespace DasaPlanet.Models
{
    public class DasaPlanetContext : DbContext
    {
        public DasaPlanetContext(DbContextOptions<DasaPlanetContext> options): base(options) {}

        public DbSet<Team> Teams { get; set; }
        public DbSet<Player> Players { get; set; }

        // Camelcased table & column names don't play well with Postgres  
        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var entity in builder.Model.GetEntityTypes())
            {
                entity.SetTableName(entity.GetTableName().ToSnakeCase());

                foreach(var property in entity.GetProperties())
                {
                    property.SetColumnName(property.GetColumnName().ToSnakeCase());
                }

                foreach (var key in entity.GetKeys())
                {
                    key.SetName(key.GetName().ToSnakeCase());
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.SetConstraintName(key.GetConstraintName().ToSnakeCase());
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.SetName(index.GetName().ToSnakeCase());
                }
            }
        }
    }
}