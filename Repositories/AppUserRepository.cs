using DasaPlanet.Models;

namespace DasaPlanet.Repositories
{
    // Not used!
    public class AppUserRepository : RepositoryBase<AppUser, AppIdentityDbContext>, IAppUserRepository
    {
        public AppUserRepository(AppIdentityDbContext appIdentityDbContext) : base(appIdentityDbContext)
        {}
    }
}