using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace DasaPlanet.Models
{
    public class AppUser : IdentityUser
    {
        public ICollection<RefreshToken> RefreshTokens { get; set; } = new List<RefreshToken>();
    }
}