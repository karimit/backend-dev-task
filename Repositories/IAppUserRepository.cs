using DasaPlanet.Models;

namespace DasaPlanet.Repositories
{
    public interface IAppUserRepository : IRepositoryBase<AppUser>
    {
        
    }
}