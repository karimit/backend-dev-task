using System;

namespace DasaPlanet.Models.Teams
{
    public class PlayerCreationModel
    {
        public string Name { get; set; }
        public string Nationality { get; set; }
        public DateTime BirthDate { get; set; }
        public string Avatar { get; set; }

        public Player ToPlayer()
        {
            return new Player
            {
                Name = Name,
                Avatar = Avatar,
                BirthDate = BirthDate,
                Nationality = Nationality
            };
        }
    }
}