using System;
using System.Text.Json.Serialization;

namespace DasaPlanet.Models
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Nationality { get; set; }
        public DateTime BirthDate { get; set; }
        public string Avatar { get; set; }

        public int TeamId { get; set; }
        [JsonIgnore]
        public Team Team { get; set; }
    }
}