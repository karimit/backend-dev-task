using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DasaPlanet.Models
{
    public class AppIdentityDbContext : IdentityDbContext<AppUser>
    {
        public DbSet<RefreshToken> RefreshToken { get; set; }

        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> options) :
            base(options)
        {
        }
    }
}