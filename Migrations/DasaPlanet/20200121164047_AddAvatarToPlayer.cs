﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DasaPlanet.Migrations.DasaPlanet
{
    public partial class AddAvatarToPlayer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "avatar",
                table: "players",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "avatar",
                table: "players");
        }
    }
}
