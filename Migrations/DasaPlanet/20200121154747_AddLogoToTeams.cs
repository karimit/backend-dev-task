﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DasaPlanet.Migrations.DasaPlanet
{
    public partial class AddLogoToTeams : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "logo",
                table: "teams",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "logo",
                table: "teams");
        }
    }
}
