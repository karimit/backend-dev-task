namespace DasaPlanet.Models.Users
{
    public class TokenRefreshModel
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}