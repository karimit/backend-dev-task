using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using DasaPlanet.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace DasaPlanet.Core
{
    public static class TokenManager
    {
        public static string GenerateOpaqueToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        public static JwtSecurityToken GenerateJwtToken(
            IConfigurationSection jwtOptions,
            AppUser user,
            IList<String> userRoles
        )
        {
            var key = Encoding.ASCII.GetBytes(jwtOptions.GetValue<String>("SecretKey"));
            var identity = new ClaimsIdentity(new GenericIdentity(user.UserName, "Token"), new[]
            {
                new Claim("id", user.Id),
                new Claim("roles", String.Join(",", userRoles))
            });

            var claims = new[]
            {
                new Claim("sub", user.Id),
                new Claim("jti", Guid.NewGuid().ToString("N")),
                new Claim(
                    "iat",
                    DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString(),
                    ClaimValueTypes.Integer64
                ),
                identity.FindFirst("roles"),
                identity.FindFirst("id")
            };

            return new JwtSecurityToken(
                issuer: jwtOptions.GetValue<String>("Issuer"),
                audience: jwtOptions.GetValue<String>("Audience"),
                claims: claims,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature
                ),
                expires: DateTime.UtcNow.AddDays(7)
            );
        }

        public static ClaimsPrincipal GetPrincipalFromToken(string token, IConfigurationSection jwtOptions)
        {
            var key = Encoding.ASCII.GetBytes(jwtOptions.GetValue<String>("SecretKey"));
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidAudience = jwtOptions.GetValue<String>("Audience"),
                ValidateIssuer = true,
                ValidIssuer = jwtOptions.GetValue<String>("Issuer"),
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateLifetime = false // Allow expired tokens
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;

            if (
                jwtSecurityToken == null ||
                !jwtSecurityToken.Header.Alg.Equals(
                    SecurityAlgorithms.HmacSha256Signature,
                    StringComparison.InvariantCultureIgnoreCase
                )
            )
                throw new SecurityTokenException("Invalid token");

            return principal;
        }

        public static string GetPrincipalIdFromToken(string token, IConfigurationSection jwtOptions)
        {
            var principal = TokenManager.GetPrincipalFromToken(token, jwtOptions);
            return principal.Claims.FirstOrDefault(c => c.Type == "id").Value;
        }
    }
}