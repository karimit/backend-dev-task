using System;
using System.Collections.Generic;

namespace DasaPlanet.Models.Teams
{
    public class TeamCreationModel
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public DateTime FoundationDate { get; set; }
        public string CoachName { get; set; }
        public string Logo { get; set; }

        public ICollection<PlayerCreationModel> Players { get; set; }
    }
}