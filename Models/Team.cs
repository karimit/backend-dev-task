using System;
using System.Collections.Generic;

namespace DasaPlanet.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public DateTime FoundationDate { get; set; }
        public string CoachName { get; set; }
        public string Logo { get; set; }
        public ICollection<Player> Players { get; set; }
    }
}