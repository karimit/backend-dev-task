using System.ComponentModel.DataAnnotations;

namespace DasaPlanet.Models.Users
{
    public class AuthenticationModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}