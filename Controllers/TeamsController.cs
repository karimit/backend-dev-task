using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DasaPlanet.Core;
using DasaPlanet.Models;
using DasaPlanet.Models.Teams;
using DasaPlanet.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DasaPlanet.Controllers
{
    [AuthorizeToken]
    [ApiController]
    [Route("[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly DasaPlanetContext _Context;
        private readonly ITeamRepository _TeamRepository;

        public TeamsController(DasaPlanetContext context, ITeamRepository teamRepository)
        {
            _Context = context;
            _TeamRepository = teamRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Team>>> GetTeams()
        {
            var teams = await _TeamRepository.ListAll();
            return Ok(teams);
        }

        [HttpGet("{id}/players")]
        public async Task<ActionResult<IEnumerable<Player>>> GetTeamPlayers(int id)
        {
            var team = await _TeamRepository.GetTeamByIdAsync(id);

            if (team == null)
            {
                return NotFound();
            }

            return team.Players.ToArray();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Team>> GetTeam(int id)
        {
            var team = await _TeamRepository.GetTeamByIdAsync(id);

            if (team == null)
            {
                return NotFound();
            }

            return team;
        }

        [AuthorizeToken(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<Team>> PostTeam([FromBody] TeamCreationModel teamModel)
        {
            var team = new Models.Team
            {
                Name = teamModel.Name,
                Country = teamModel.Country,
                CoachName = teamModel.Country,
                FoundationDate = teamModel.FoundationDate,
                Logo = teamModel.Logo,
                Players = teamModel.Players.Select(p => p.ToPlayer()).ToArray()
            };

            _TeamRepository.Create(team);
            // All changes in a single call to SaveChanges() are applied in a transaction
            await _Context.SaveChangesAsync();

            return CreatedAtAction("GetTeam", new { id = team.Id }, team);
        }

        [AuthorizeToken(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTeam(int id, Team team)
        {
            if (id != team.Id)
            {
                return BadRequest();
            }

            _Context.Entry(team).State = EntityState.Modified;

            try
            {
                await _Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(await _TeamRepository.ExistAsync(t => t.Id == id)))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [AuthorizeToken(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Team>> DeleteTeam(int id)
        {
            var team = await _TeamRepository.GetTeamByIdAsync(id);
            if (team == null)
            {
                return NotFound();
            }

            _TeamRepository.Delete(team);
            await _Context.SaveChangesAsync();

            return team;
        }
    }
}