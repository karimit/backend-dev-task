﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DasaPlanet.Migrations
{
    public partial class AddRefreshTokensAppUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "RefreshToken");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "RefreshToken",
                type: "text",
                nullable: true);
        }
    }
}
