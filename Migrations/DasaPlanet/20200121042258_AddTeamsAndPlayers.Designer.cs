﻿// <auto-generated />
using System;
using DasaPlanet.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DasaPlanet.Migrations.DasaPlanet
{
    [DbContext(typeof(DasaPlanetContext))]
    [Migration("20200121042258_AddTeamsAndPlayers")]
    partial class AddTeamsAndPlayers
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("DasaPlanet.Models.Player", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("BirthDate")
                        .HasColumnName("birth_date")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Name")
                        .HasColumnName("name")
                        .HasColumnType("text");

                    b.Property<string>("Nationality")
                        .HasColumnName("nationality")
                        .HasColumnType("text");

                    b.Property<int>("TeamId")
                        .HasColumnName("team_id")
                        .HasColumnType("integer");

                    b.HasKey("Id")
                        .HasName("pk_players");

                    b.HasIndex("TeamId")
                        .HasName("ix_players_team_id");

                    b.ToTable("players");
                });

            modelBuilder.Entity("DasaPlanet.Models.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("CoachName")
                        .HasColumnName("coach_name")
                        .HasColumnType("text");

                    b.Property<string>("Country")
                        .HasColumnName("country")
                        .HasColumnType("text");

                    b.Property<DateTime>("FoundationDate")
                        .HasColumnName("foundation_date")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Name")
                        .HasColumnName("name")
                        .HasColumnType("text");

                    b.HasKey("Id")
                        .HasName("pk_teams");

                    b.ToTable("teams");
                });

            modelBuilder.Entity("DasaPlanet.Models.Player", b =>
                {
                    b.HasOne("DasaPlanet.Models.Team", "Team")
                        .WithMany("Players")
                        .HasForeignKey("TeamId")
                        .HasConstraintName("fk_players_teams_team_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
