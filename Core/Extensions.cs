using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Identity;

namespace DasaPlanet.Core
{
    public static class DasaPlanet
    {
        public static string ToSnakeCase(this string text)
        {
            if (String.IsNullOrEmpty(text)) return text;

            var startUnderscores = Regex.Match(text, @"^_+");
            return startUnderscores + Regex.Replace(text, @"([a-z0-9])([A-Z])", "$1_$2").ToLower();
        }

        public static string AggregateErrors(this IEnumerable<IdentityError> errors)
        {
            return errors?.ToList()
                          .Select(f => f.Description)
                          .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");
        }
    }
}