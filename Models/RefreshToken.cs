using System;
using DasaPlanet.Core;

namespace DasaPlanet.Models
{
    public class RefreshToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime Expires { get; set; }
        public string AppUserId { get; set; }
        public bool Active => DateTime.UtcNow <= Expires;

        public static RefreshToken Build(
            string userId,
            int expiryDays = 7
        )
        {
            return new RefreshToken
            {
                AppUserId = userId,
                Token = TokenManager.GenerateOpaqueToken(),
                Expires = DateTime.UtcNow.AddDays(expiryDays)
            };
        }
    }
}