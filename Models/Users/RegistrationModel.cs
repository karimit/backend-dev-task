using System.ComponentModel.DataAnnotations;

namespace DasaPlanet.Models.Users
{
    public class RegistrationModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}