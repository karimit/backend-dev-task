using System.Threading.Tasks;
using DasaPlanet.Models;
using Microsoft.EntityFrameworkCore;

namespace DasaPlanet.Repositories
{
    public class PlayerRepository : RepositoryBase<Player, DasaPlanetContext>, IPlayerRepository
    {
        public PlayerRepository(DasaPlanetContext dasaPlanetContext) : base(dasaPlanetContext)
        {
        }

        public async Task<Player> GetPlayerByIdAsync(int PlayerId)
        {
            return await FindByCondition(Player => Player.Id.Equals(PlayerId)).
                            FirstOrDefaultAsync();
        }
    }
}