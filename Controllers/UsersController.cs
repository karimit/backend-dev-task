using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using DasaPlanet.Core;
using DasaPlanet.Models;
using DasaPlanet.Models.Users;
using DasaPlanet.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DasaPlanet.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        protected UserManager<AppUser> _UserManager;
        private readonly AppIdentityDbContext _IdentityDbContext;
        private readonly IAppUserRepository _AppUserRepository;
        private readonly IConfiguration _Configuration;
        
        private IConfigurationSection jwtOptions;
        private IConfigurationSection _JwtOptions
        {
            get
            {
                return jwtOptions ??= _Configuration.GetSection("Jwt");
            }
        }

        public UsersController(
            UserManager<AppUser> userManager,
            AppIdentityDbContext identityDbContext,
            IAppUserRepository appUserRepository,
            IConfiguration configuration
        )
        {
            _UserManager = userManager;
            _IdentityDbContext = identityDbContext;
            _AppUserRepository = appUserRepository;
            _Configuration = configuration;
        }

        [AllowAnonymous]
        [Route("register")]
        public async Task<ActionResult<RegistrationResult>> RegisterAsync(
            [FromBody]RegistrationModel registrationCredentials
        )
        {
            if (registrationCredentials == null ||
                String.IsNullOrWhiteSpace(registrationCredentials.Email))
            {
                return BadRequest(
                    new { message = "Please provide all required details to register for an account" }
                );
            }

            var user = new AppUser
            {
                Email = registrationCredentials.Email,
                UserName = registrationCredentials.UserName,
                EmailConfirmed = true
            };

            var result = await _UserManager.CreateAsync(
                user,
                registrationCredentials.Password
            );

            if (result.Succeeded)
            {
                var userIdentity = await _UserManager.FindByNameAsync(user.UserName);

                // Return valid response containing all users details
                return new RegistrationResult
                {
                    Email = userIdentity.Email,
                    UserName = userIdentity.UserName
                };
            }
            else
            {
                return BadRequest(new { message = result.Errors.AggregateErrors() });
            }
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateAsync([FromBody]AuthenticationModel authenticationModel)
        {
            var user = await _UserManager.FindByNameAsync(authenticationModel.Username);
            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            if (await _UserManager.CheckPasswordAsync(user, authenticationModel.Password))
            {
                var token = await GenerateTokenForUserAsync(user);
                var refreshToken = RefreshToken.Build(user.Id);

                user.RefreshTokens.Add(refreshToken);
                _IdentityDbContext.Entry(user).State = EntityState.Modified;
                await _IdentityDbContext.SaveChangesAsync();

                return Ok(new AuthenticationResult
                {
                    Email = user.Email,
                    UserName = user.UserName,
                    Token = new JwtSecurityTokenHandler().WriteToken(token),
                    RefreshToken = refreshToken.Token
                });
            }
            else
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
        }

        [AllowAnonymous]
        [HttpPost("refreshtoken")]
        public async Task<IActionResult> RefreshTokenAsync([FromBody] TokenRefreshModel tokenRefreshModel)
        {
            var userId = TokenManager.GetPrincipalIdFromToken(tokenRefreshModel.Token, _JwtOptions);

            var principalRefreshToken = userId != null ?
                await _IdentityDbContext.RefreshToken.FirstOrDefaultAsync(
                    rt => rt.AppUserId == userId && rt.Token == tokenRefreshModel.RefreshToken
                ) : null;

            if (principalRefreshToken == null || principalRefreshToken.Token != tokenRefreshModel.RefreshToken)
                return BadRequest(new { message = "Invalid token" });

            var user = await _UserManager.FindByIdAsync(userId);
            var newToken = await GenerateTokenForUserAsync(user);
            var newRefreshToken = RefreshToken.Build(userId);

            _IdentityDbContext.RefreshToken.Remove(principalRefreshToken);
            user.RefreshTokens.Add(newRefreshToken);
            _IdentityDbContext.Entry(user).State = EntityState.Modified;
            await _IdentityDbContext.SaveChangesAsync();

            return Ok(new AuthenticationResult
            {
                Email = user.Email,
                UserName = user.UserName,
                Token = new JwtSecurityTokenHandler().WriteToken(newToken),
                RefreshToken = newRefreshToken.Token
            });
        }

        private async Task<JwtSecurityToken> GenerateTokenForUserAsync(AppUser user)
        {
            var userRoles = await _UserManager.GetRolesAsync(user);
            return TokenManager.GenerateJwtToken(_JwtOptions, user, userRoles);
        }
    }
}