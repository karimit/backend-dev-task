namespace DasaPlanet.Models.Users
{
    public class RegistrationResult
    {
        public string Email { get; set; }
        public string UserName { get; set; }
    }
}