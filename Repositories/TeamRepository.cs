using System.Threading.Tasks;
using DasaPlanet.Models;
using Microsoft.EntityFrameworkCore;

namespace DasaPlanet.Repositories
{
    public class TeamRepository : RepositoryBase<Team, DasaPlanetContext>, ITeamRepository
    {
        public TeamRepository(DasaPlanetContext dasaPlanetContext) : base(dasaPlanetContext)
        {
        }

        public async Task<Team> GetTeamByIdAsync(int teamId)
        {
            return await FindByCondition(team => team.Id.Equals(teamId)).
                            Include(t => t.Players).
                            FirstOrDefaultAsync();
        }
    }
}