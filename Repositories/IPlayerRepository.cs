using System.Threading.Tasks;
using DasaPlanet.Models;

namespace DasaPlanet.Repositories
{
    public interface IPlayerRepository : IRepositoryBase<Player>
    {
        Task<Player> GetPlayerByIdAsync(int PlayerId);
    }
}