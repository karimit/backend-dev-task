# Backend Dev Task

## Notes:
* Set the correct database user & password in __ConnectionStrings.DefaultConnection__ entry in __appsettings.json__
* Run <br/>
    `dotnet ef database update --context AppIdentityDbContext`
* Run <br/>
    `dotnet ef database update --context DasaPlanetContext`
* A user with the __Admin__ role gets created on application startup. The `Admin` section of `appsettings.json` contains their credentials.
* A __Postman__ collection is provided to quickly test available endpoints. [postman-collection](DasaPlanet.postman_collection.json)
* I worked for ~10 hours on the backend.
