using System.Threading.Tasks;
using DasaPlanet.Models;

namespace DasaPlanet.Repositories
{
    public interface ITeamRepository : IRepositoryBase<Team>
    {
        Task<Team> GetTeamByIdAsync(int teamId);
    }
}