namespace DasaPlanet.Models.Users
{
    public class AuthenticationResult
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}